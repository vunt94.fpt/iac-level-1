terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
  backend "s3" {}
}

provider "aws" {
  region = "us-east-2"
}

data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = "vunt-bucket"
    key    = "terraform.tfstate"
    region = "us-east-2"
  }
}

output "vpc_id" {
  value = data.terraform_remote_state.vpc.outputs.vpc_id
}

locals {
  vpc_id = data.terraform_remote_state.vpc.outputs.vpc_id
  nsg_id = data.terraform_remote_state.vpc.outputs.nsg_id
  subnet_id = data.terraform_remote_state.vpc.outputs.subnet_id
}

resource "aws_instance" "web" {
  # name = "vunt-ec2"
  ami           = "ami-02a89066c48741345" # us-west-2
  instance_type = "t2.micro"
  vpc_security_group_ids = [local.nsg_id]
  subnet_id              = local.subnet_id
  # key_name = "vunt-keypair.pem"
  tags = {
    Name = "vunt"
  }
  
}